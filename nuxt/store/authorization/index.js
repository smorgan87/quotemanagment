// import axios from 'axios';
// import Cookies from 'js-cookie'
import _ from 'lodash'
import jwtDecode from 'jwt-decode'
// import { apiAxios } from '~/plugins/axios'

const AuthorizationStore = {
  state: {
    email: undefined,
    jwt: undefined,
    jwtExpiry: undefined
  },
  mutations: {
    SET_EMAIL(state, payload) {
      state.email = payload
    },
    SET_JWT(state, payload) {
      state.jwt = payload
    },
    SET_JWT_EXPIRY(state, payload) {
      state.jwtExpiry = payload
    }
  },
  actions: {
    // checkAuthenticationStatus(vuexContext) {
    //   console.log(Date.now())
    //   const jwt = Cookies.get('r4v3n-jwt')
    //   if (!_.isUndefined(jwt)) {
    //     try {
    //       const token = jwtDecode(jwt)
    //       const tokenExpiresAt = token.exp
    //       const currentTime = Date.now()
    //       console.log(token, tokenExpiresAt - currentTime)
    //     } catch (error) {
    //       console.log('Bad Dog. Sit.')
    //       return false
    //     }
    //   }
    //   //   if(!_.isUndefined(jwt)){

    //   //   }
    //   //   return false;
    //   //   const jwt = Cookies.get('r4v3n-jwt')
    //   //   console.log(
    //   //     'test',
    //   //     email,
    //   //     jwt,
    //   //     !_.isUndefined(email) && !_.isUndefined(jwt)
    //   //   )
    //   //   if (!_.isUndefined(email) && !_.isUndefined(jwt)) {
    //   //     vuexContext.commit('SET_EMAIL', email)
    //   //     vuexContext.commit('SET_JWT', jwt)
    //   //     return true
    //   //   } else {
    //   //     return false
    //   //   }
    // },
    signup(vuexContext, { email, password }) {
      this.$axios
        .post('auth/signup', {
          email,
          password
        })
        .then((response) => {
          console.log(response)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    login(vuexContext, { email, password }) {
      return this.$axios
        .post(
          'auth/signin',
          {
            email,
            password
          },
          { withCredentials: true }
        )
        .then((response) => {
          console.info('[LOGIN_RESPONSE]: ', response)
          const jwt = response.data.accessToken
          vuexContext.commit('SET_EMAIL', email)
          vuexContext.commit('SET_JWT', jwt)
          // Cookies.set('r4v3n-jwt', jwt, { expires: 1 }
          this.$startSilentJWTRefresh(jwt)
          return true
        })
        .catch((error) => {
          console.log(error)
          return false
        })
    },
    refresh(vuexContext) {
      return this.$axios
        .post('auth/refresh', null, { withCredentials: true })
        .then((response) => {
          console.info('[REFRESH_RESPONSE]: ', response)
          const jwt = response.data.accessToken
          const decoded = jwtDecode(jwt)
          const { email } = decoded
          vuexContext.commit('SET_EMAIL', email)
          vuexContext.commit('SET_JWT', jwt)
          // Cookies.set('r4v3n-jwt', jwt, { expires: 1 })
          this.$startSilentJWTRefresh(jwt)
          return true
        })
        .catch((error) => {
          console.log(error)
          return false
        })
    },
    logout(vuexContext) {
      return this.$axios
        .post('auth/logout', null, { withCredentials: true })
        .then((response) => {
          console.log(vuexContext)
          this.$stopSilentJWTRefresh()
        })
        .catch((error) => {
          console.log(error)
          return false
        })

      //   vuexContext.commit('SET_EMAIL', undefined)
      //   vuexContext.commit('SET_JWT', undefined)
      //   Cookies.remove('r4v3n-jwt')
      // window.location.href('/')
    }
  },
  getters: {
    clientEmail(state, getters, rootState) {
      return state.email
    },
    jwt(state, getters, rootState) {
      return state.jwt
    },
    jwtExpiry(state, getters, rootState) {
      return state.jwtExpiry
    },
    isLoggedIn(state, getters, rootState) {
      return !_.isUndefined(getters.jwt)
    }
  }
}

export default AuthorizationStore
