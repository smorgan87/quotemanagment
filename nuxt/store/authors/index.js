const AuthorsStore = {
  state: {
    authors: []
  },
  mutations: {
    SET_AUTHORS(state, payload) {
      state.authors = payload
    }
  },
  actions: {
    getAuthors({ getters, commit, dispatch }) {
      this.$axios
        .get('authors')
        .then((response) => {
          commit('SET_AUTHORS', response.data)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    CREATE_AUTHOR(context, payload) {
      this.$axios
        .post('authors', payload)
        .then((response) => {
          const { data } = response
          const authors = context.getters.authors
          authors.push(data)
          authors.sort((a, b) => {
            if (a.firstname > b.firstname) {
              return 1
            } else if (a.firstname < b.firstname) {
              return -1
            }
            if (a.lastname < b.lastname) {
              return -1
            } else if (a.lastname > b.lastname) {
              return 1
            } else {
              return 0
            }
          })
          context.commit('SET_AUTHORS', authors)
        })
        .catch((error) => {
          console.log(error)
        })
    }
  },
  getters: {
    authors(state, getters, rootState) {
      return state.authors
    }
  }
}

export default AuthorsStore
