const UIFeedback = {
  state: {
    toast: undefined
  },
  mutations: {
    SET_TOAST(state, payload) {
      state.toast = payload
    }
  },
  actions: {
    SET_TOAST(vuexContext, payload) {
      vuexContext.commit('SET_TOAST', payload)
      setTimeout(() => {
        console.log('clear toast')
        vuexContext.commit('SET_TOAST', undefined)
      }, 3000)
    }
  },
  getters: {
    toast(state, getters, rootState) {
      return state.toast
    }
  }
}

export default UIFeedback
