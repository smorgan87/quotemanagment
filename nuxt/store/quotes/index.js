// import axios from 'axios';
// import Cookies from 'js-cookie'
import _ from 'lodash'
//  import jwtDecode from 'jwt-decode'
// import api from '@/store/apiConfig'
// import { apiAxios } from '~/plugins/axios'

const QuotesStore = {
  state: {
    quotes: []
  },
  mutations: {
    SET_QUOTES(state, payload) {
      state.quotes = payload
    }
  },
  actions: {
    getQuotes({ getters, commit, dispatch }) {
      // console.log(vuexContext)
      // apiAxios.defaults.headers.common.Authorization = `Bearer ${getters.jwt}`
      // apiAxios.interceptors.response.use(
      //   function(response) {
      //     // Any status code that lie within the range of 2xx cause this function to trigger
      //     // Do something with response data
      //     console.log('Response Interceptor response', response)
      //     return response
      //   },
      //   function(error) {
      //     // Any status codes that falls outside the range of 2xx cause this function to trigger
      //     // Do something with response error
      //     // console.log('Response Interceptor error', error.response, error)
      //     if (error.response.status === 401) dispatch('logout')
      //     return Promise.reject(error)
      //   }
      // )
      this.$axios
        .get('quotes')
        .then((response) => {
          // console.log(response)
          // vuexContext.mutations
          commit('SET_QUOTES', response.data)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    createQuote({ getters, commit, dispatch }, quote) {
      this.$axios
        .post('quotes', quote)
        .then((response) => {
          const quotes = getters.quotes

          quotes.unshift(response.data)

          commit('SET_QUOTES', quotes)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    addNote({ getters, commit, dispatch }, data) {
      this.$axios
        .post(`quotes/${data.quoteId}/note`, { note: data.note })
        .then((response) => {
          // Update Notes within Quote
          const quotes = getters.quotes
          const quote = _.find(quotes, { id: data.quoteId })
          if (quote.note) {
            quote.note.unshift(response.data)
          } else {
            quote.note = [response.data]
          }
          console.log(quote)
          const quoteIndex = _.findIndex(quotes, { id: data.quoteId })
          quotes.splice(quoteIndex, 1, { ...quote })
          console.log('updated quotes', quotes)
          commit('SET_QUOTES', quotes)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    editNote({ getters, commit, dispatch }, data) {
      this.$axios
        .put(`notes`, { ...data.payload })
        .then((response) => {
          const updatedNote = response.data
          // Update Notes within Quote
          const quotes = getters.quotes
          const quote = _.find(quotes, { id: data.quoteId })
          const notes = quote.note
          const noteIndex = _.findIndex(notes, { id: updatedNote.id })
          notes.splice(noteIndex, 1, { ...updatedNote })
          console.log('newnotes', notes)
          commit('SET_QUOTES', quotes)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    deleteNote({ getters, commit, dispatch }, data) {
      this.$axios
        .delete(`notes/${data.id}`)
        .then((response) => {
          // Update Notes within Quote
          const quotes = getters.quotes
          const quote = _.find(quotes, { id: data.quoteId })
          const notes = quote.note
          const noteIndex = _.findIndex(notes, { id: data.id })
          notes.splice(noteIndex, 1)
          commit('SET_QUOTES', quotes)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    addTag({ getters, commit, dispatch }, data) {
      this.$axios
        .post(`tags/${data.quoteId}`, { name: data.name })
        .then((response) => {
          console.log(response)
          // Update Tags within Quote
          const quotes = getters.quotes
          const quote = _.find(quotes, { id: data.quoteId })
          if (quote.tags) {
            console.log('hey1', quote.tags)
            quote.tags.push(response.data)
            quote.tags.sort((a, b) => {
              const nameA = a.name.toUpperCase() // ignore upper and lowercase
              const nameB = b.name.toUpperCase() // ignore upper and lowercase
              if (nameA < nameB) {
                return -1
              }
              if (nameA > nameB) {
                return 1
              }

              // names must be equal
              return 0
            })
            console.log('hey2', quote.tags)
          } else {
            quote.tags = [response.data]
          }
          // console.log(quote)
          const quoteIndex = _.findIndex(quotes, { id: data.quoteId })
          quotes.splice(quoteIndex, 1, { ...quote })
          console.log('updated quotes', quotes)
          dispatch('SET_TOAST', 'Tag Applied')
          commit('SET_QUOTES', quotes)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    deleteTag({ getters, commit, dispatch }, data) {
      this.$axios
        .delete(`tags/${data.id}/quote/${data.quoteId}`)
        .then((response) => {
          // Update Notes within Quote
          const quotes = getters.quotes
          const quote = _.find(quotes, { id: data.quoteId })
          const tags = quote.tags
          const tagIndex = _.findIndex(tags, { id: data.id })
          tags.splice(tagIndex, 1)
          commit('SET_QUOTES', quotes)
          dispatch('SET_TOAST', `Tag Removed`)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    editTag({ getters, commit, dispatch }, data) {
      this.$axios
        .patch(`tags/${data.payload.id}`, { name: data.payload.name })
        .then((response) => {
          // Update Notes within Quote
          const quotes = getters.quotes
          _.forEach(quotes, (quote) => {
            const tags = quote.tags
            const tagIndex = _.findIndex(tags, { id: data.payload.id })
            if (tagIndex !== -1) {
              const updatedTag = { ...tags[tagIndex], name: data.payload.name }
              tags.splice(tagIndex, 1, updatedTag)
              console.log('new quotes', quotes)
            }
          })
          commit('SET_QUOTES', quotes)
          dispatch('SET_TOAST', 'Tag Updated')
        })
        .catch((error) => {
          console.log(error)
        })
    }
  },
  getters: {
    quotes(state, getters, rootState) {
      return state.quotes
    }
  }
}

export default QuotesStore
