// import https from 'https'
import Vuex from 'vuex'
// import axios from 'axios'
// import { apiAxios } from '~/plugins/axios'
// import Cookies from 'js-cookie'
import AuthorizationStoreModule from './authorization'
import AuthorsModule from './authors'
import SourcesModule from './sources'
import QuotesStoreModule from './quotes'
import UIFeedback from './ui-feedback'
import Categories from './categories' // @TODO : <--- Move to Standard

const createStore = () => {
  return new Vuex.Store({
    state: {},
    mutations: {},
    actions: {
      nuxtServerInit(vuexContext, context) {
        // Initial Load
      }
    },
    getters: {},
    modules: {
      authorization: AuthorizationStoreModule,
      authors: AuthorsModule,
      sources: SourcesModule,
      quotes: QuotesStoreModule,
      uifeedback: UIFeedback,
      Categories
    }
  })
}

export default createStore
