import _ from 'lodash'
import Logger from 'js-logger'

Logger.useDefaults()
const logger = Logger.get('Categories Store')

const Categories = {
  // state: {
  //   toast: undefined
  // },
  // mutations: {
  //   SET_TOAST(state, payload) {
  //     state.toast = payload
  //   }
  // },
  actions: {
    ADD_CATEGORY_TO_QUOTE({ getters, dispatch, commit }, payload) {
      logger.info('Add Category to Quote: ', payload)
      this.$axios
        .post(`categories/${payload.quoteId}`, { name: payload.name })
        .then((response) => {
          logger.info('Add Category to Quote Response: ', payload)
          const quotes = getters.quotes
          const quote = _.find(quotes, { id: payload.quoteId })
          if (quote.categories) {
            quote.categories.push(response.data)
            quote.categories.sort((a, b) => {
              const nameA = a.name.toUpperCase() // ignore upper and lowercase
              const nameB = b.name.toUpperCase() // ignore upper and lowercase
              if (nameA < nameB) {
                return -1
              }
              if (nameA > nameB) {
                return 1
              }
              // names must be equal
              return 0
            })
          } else {
            quote.categories = [response.data]
          }
          const quoteIndex = _.findIndex(quotes, { id: payload.quoteId })
          quotes.splice(quoteIndex, 1, { ...quote })
          dispatch('SET_TOAST', 'Category Applied')
          commit('SET_QUOTES', quotes)
        })
        .catch((error) => {
          logger.error(error)
        })
    },
    EDIT_CATEGORY_ON_QUOTE({ getters, dispatch, commit }, data) {
      logger.info('Edit Category.', data)
      this.$axios
        .patch(`categories/${data.payload.id}`, { name: data.payload.name })
        .then((response) => {
          // Update Notes within Quote
          const quotes = getters.quotes
          _.forEach(quotes, (quote) => {
            const categories = quote.categories
            const categoryIndex = _.findIndex(categories, {
              id: data.payload.id
            })
            if (categoryIndex !== -1) {
              const updatedTag = {
                ...categories[categoryIndex],
                name: data.payload.name
              }
              categories.splice(categoryIndex, 1, updatedTag)
            }
          })
          commit('SET_QUOTES', quotes)
          dispatch('SET_TOAST', 'Category Updated')
        })
        .catch((error) => {
          console.log(error)
        })
    },
    REMOVE_CATEGORY_FROM_QUOTE({ getters, dispatch, commit }, payload) {
      logger.info('Remove Category from Quote.', payload)
      this.$axios
        .delete(`categories/${payload.id}/quote/${payload.quoteId}`)
        .then((response) => {
          const quotes = getters.quotes
          const quote = _.find(quotes, { id: payload.quoteId })
          const categories = quote.categories
          const categoryIndex = _.findIndex(categories, { id: payload.id })
          categories.splice(categoryIndex, 1)
          commit('SET_QUOTES', quotes)
          dispatch('SET_TOAST', `Category Removed`)
        })
        .catch((error) => {
          logger.error(error)
        })
    }
  },
  getters: {
    toast(state, getters, rootState) {
      return state.toast
    }
  }
}

export default Categories
