export default function({ $axios, redirect, store, router }) {
  $axios.onRequest((config) => {
    console.log('[AXIOS_PLUGIN] On Request: ', config)
    config.headers.common.Authorization = `Bearer ${store.getters.jwt}`
  })
  $axios.onResponse((config) => {
    console.log('[AXIOS_PLUGIN] On Response: ', config)
  })
  $axios.onError((error) => {
    console.log('[AXIOS_PLUGIN] On Error: ', error.response)
    const status = error.response.status
    if (status === 401) redirect('/')
  })
}
