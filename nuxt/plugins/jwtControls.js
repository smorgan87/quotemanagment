import jwtDecode from 'jwt-decode'

let timer

export default ({ app }, inject) => {
  inject('startSilentJWTRefresh', (token) => {
    const buffer = 10
    const decoded = jwtDecode(token)
    const currentTime = Date.now() / 1000
    const refreshAt = (Math.round(decoded.exp - currentTime) - buffer) * 1000
    // if (remainingTime < 0) throw new Error('Uht Oh!')
    console.log(refreshAt, Math.round(decoded.exp - currentTime) - buffer)
    timer = setTimeout(() => {
      console.log('timeToRefresh!', app, decoded.email)
      app.store.dispatch('refresh')
    }, refreshAt)
  })
  inject('stopSilentJWTRefresh', () => {
    clearTimeout(timer)
  })
}
