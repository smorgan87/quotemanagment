export default function({ store, redirect }) {
  console.log('[MIDDLEWARE_AUTHENTICATION]', store.app.store.getters.isLoggedIn)
  if (!store.app.store.getters.isLoggedIn) return redirect('/')
}
