# Raven 

A simple quote managment application.

## Technologies

### Reverse Proxy : Nginx
[Nginx](https://www.nginx.com/) : A web server that can also be used as a reverse proxy, load balancer, mail proxy and HTTP cache. 

### Database : Postgres
[Postgres](https://www.postgresql.org/) : A free and open-source relational database management system emphasizing extensibility and SQL compliance.

### Backend Application : NestJS
[NestJS](https://nestjs.com/): A progressive Node.js framework for building efficient, reliable and scalable server-side applications.

### Frontend Application : NuxtJS
[NuxtJS](https://nuxtjs.org/) : A progressive framework based on Vue.js to create modern web applications. 

### Containerization : Docker
[Docker](https://www.docker.com/) : A set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. 

## Getting Started

```
docker-compose up -d
```

Application will be available [localhost](http://localhost/). Hot-Reloads & a Postgres Container w/ Volume included for local development  🚀

### Dev Database Interactions

Connect to database using env vars
```
docker exec -it <container_name> psql -U<user_name> -a <db_name>
```
e.g. default
```
docker exec -it r4v3n-database psql -Ur4v3n -a r4v3n 
```

### Terminate Dev Environment

```
docker-compose down
```

### Clear Docker Volumes

References: [Docker Remove Volume](https://docs.docker.com/engine/reference/commandline/volume_rm/)

```
docker volume rm ${Project Name}_data ${Project Name}_app
```

## Creating a New Nest Module

Ensure you are in the correct folder

```
cd nest
```

Utilize the nest cli

```
nest g module ${MODULE_NAME}
```

Create the entity



## Build

🔧 Coming Soon... 🔧

## Environment Variables
Currently set within docker-compose.yml at the projects root.