import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  Unique,
  OneToMany,
} from 'typeorm';
import * as bycrypt from 'bcrypt';
import { Quote } from 'src/quotes/quote.entity';
import { Author } from 'src/authors/author.entity';
import { Note } from 'src/notes/note.entity';
import { Source } from 'src/sources/source.entity';

@Entity()
@Unique(['email'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  salt: string;

  @OneToMany(
    quote => Quote,
    quote => quote.user,
    { eager: true },
  )
  quote: Quote[];

  @OneToMany(
    author => Author,
    author => author.user,
    { eager: true },
  )
  author: Author[];

  @OneToMany(
    note => Note,
    note => note.user,
    { eager: false },
  )
  note: Note[];

  @OneToMany(
    source => Source,
    source => source.user,
    { eager: true },
  )
  source: Source[];

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bycrypt.hash(password, this.salt);
    return hash === this.password;
  }
}
