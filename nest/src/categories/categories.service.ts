import { Injectable, NotFoundException } from '@nestjs/common';
import { CategoryRepository } from './category.repository';
import { GetCategoryFilterDto } from './dto/get-categories-filter-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/user.entity';
import { Category } from './category.entity';
import { CreateCategoryDto } from './dto/create-category-dto';
import { UpdateCategoryDto } from './dto/update-category-dto';
import { QuoteRepository } from 'src/quotes/quote.repository';
import * as _ from 'lodash';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(CategoryRepository)
    private categoryRepository: CategoryRepository,
    @InjectRepository(QuoteRepository)
    private quoteRepository: QuoteRepository,
  ) {}

  async getCategories(
    filterDto: GetCategoryFilterDto,
    user: User,
  ): Promise<Category[]> {
    return this.categoryRepository.getCategories(filterDto, user);
  }

  async getCategoryById(id: number, user: User): Promise<Category> {
    const category = await this.categoryRepository.findOne({
      where: { id, userId: user.id },
    });
    if (!category) throw new NotFoundException(`Category with id id not found`);
    return category;
  }

  async createCategory(createCategoryDto: CreateCategoryDto, user: User) {
    return this.categoryRepository.createCategory(createCategoryDto, user);
  }

  async newCategoryOnQuote(createCategoryDto: CreateCategoryDto, quoteId: number, user: User) {
    const { name } = createCategoryDto;
    console.log('Previous Category Name: ', name);
    let category = await this.categoryRepository.findOne({
      where: { name, userId: user.id },
    });
    console.log('Previous Category: ', category);
    if (!category) {
      category = await this.categoryRepository.newCategoryOnQuote(createCategoryDto, user);
    }
    const quote = await this.quoteRepository.findOne({
      where: { id: quoteId, userId: user.id },
      relations: ['categories'],
    });
    if (!quote)
      throw new NotFoundException(`Quote with id ${quoteId} not found`);
    const categories = quote.categories;
    categories.push(category);
    quote.categories = categories;
    quote.save();
    return category;
  }

  async updateCategory(id: number, updateCategoryDto: UpdateCategoryDto, user: User) {
    const { name } = updateCategoryDto;
    const category = await this.categoryRepository
      .createQueryBuilder()
      .update('category')
      .set({ name })
      .where('id = :id', { id })
      .execute();
    return category;
  }

  async deleteCategoryById(id: number, user: User): Promise<void> {
    const result = await this.categoryRepository.delete({
      id,
      userId: user.id,
    });
    if (!(await result).affected)
      throw new NotFoundException(`Category with id ${id} not found`);
  }

  async deleteCategoryFromQuote(id: number, quoteId: number, user: User): Promise<void> {
    const quote = await this.quoteRepository.findOne({
      where: { id: quoteId, userId: user.id },
      relations: ['categories'],
    });
    if (!quote)
      throw new NotFoundException(`Quote with id ${quoteId} not found`);
    const categories = quote.categories;
    console.log(quote.categories, _);
    const categoryIndex = _.findIndex(categories, { id });
    if (!categoryIndex) throw new NotFoundException(`Category with id ${id} not found`);
    categories.splice(categoryIndex, 1);
    quote.categories = categories;
    quote.save();
  }
}
