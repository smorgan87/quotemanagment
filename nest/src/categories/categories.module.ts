import { Module } from '@nestjs/common';
import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryRepository } from './category.repository';
import { AuthModule } from 'src/auth/auth.module';
import { QuoteRepository } from 'src/quotes/quote.repository';

@Module({
  imports: [TypeOrmModule.forFeature([CategoryRepository, QuoteRepository]), AuthModule],
  controllers: [CategoriesController],
  providers: [CategoriesService],
})
export class CategoriesModule {}
