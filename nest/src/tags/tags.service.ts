import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/user.entity';
import { TagRepository } from './tags.repository';
import { GetTagFilterDto } from './dto/get-tag-filter-dto';
import { CreateTagDto } from './dto/create-tag-dto';
import { Tag } from './tag.entity';
import { UpdateTagDto } from './dto/update-tag-dto';
import { QuoteRepository } from 'src/quotes/quote.repository';
import * as _ from 'lodash';

@Injectable()
export class TagsService {
  constructor(
    @InjectRepository(TagRepository)
    private tagRepository: TagRepository,
    @InjectRepository(QuoteRepository)
    private quoteRepository: QuoteRepository,
  ) {}

  async getTags(filterDto: GetTagFilterDto, user: User): Promise<Tag[]> {
    return this.tagRepository.getTags(filterDto, user);
  }

  async getTagById(id: number, user: User): Promise<Tag> {
    const tag = await this.tagRepository.findOne({
      where: { id, userId: user.id },
    });
    if (!tag) throw new NotFoundException(`Tag with id id not found`);
    return tag;
  }

  async createTag(createTagDto: CreateTagDto, user: User) {
    return this.tagRepository.createTag(createTagDto, user);
  }

  async newTagOnQuote(createTagDto: CreateTagDto, quoteId: number, user: User) {
    const { name } = createTagDto;
    console.log('Previous Tag Name: ', name);
    let tag = await this.tagRepository.findOne({
      where: { name, userId: user.id },
    });
    console.log('Previous Tag: ', tag);
    if (!tag) {
      tag = await this.tagRepository.newTagOnQuote(createTagDto, user);
    }
    const quote = await this.quoteRepository.findOne({
      where: { id: quoteId, userId: user.id },
      relations: ['tags'],
    });
    if (!quote)
      throw new NotFoundException(`Quote with id ${quoteId} not found`);
    const tags = quote.tags;
    tags.push(tag);
    quote.tags = tags;
    quote.save();
    return tag;
  }

  async updateTag(id: number, updateTagDto: UpdateTagDto, user: User) {
    const { name } = updateTagDto;
    const tag = await this.tagRepository
      .createQueryBuilder()
      .update('tag')
      .set({ name })
      .where('id = :id', { id })
      .execute();
    return tag;
  }

  async deleteTagById(id: number, quoteId: number, user: User): Promise<void> {
    const quote = await this.quoteRepository.findOne({
      where: { id: quoteId, userId: user.id },
      relations: ['tags'],
    });
    if (!quote)
      throw new NotFoundException(`Quote with id ${quoteId} not found`);
    const tags = quote.tags;
    console.log(quote.tags, _);
    const tagIndex = _.findIndex(tags, { id });
    if (!tagIndex) throw new NotFoundException(`Tag with id ${id} not found`);
    tags.splice(tagIndex, 1);
    quote.tags = tags;
    quote.save();
    // return tag;
    // If last tag instance on quote full delete
    // const result = await this.tagRepository.delete({
    //   id,
    //   userId: user.id,
    // });
  }
}
