import { Module } from '@nestjs/common';
import { TagsController } from './tags.controller';
import { TagsService } from './tags.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TagRepository } from './tags.repository';
import { AuthModule } from 'src/auth/auth.module';
import { QuoteRepository } from 'src/quotes/quote.repository';

@Module({
  imports: [TypeOrmModule.forFeature([TagRepository, QuoteRepository]), AuthModule],
  controllers: [TagsController],
  providers: [TagsService],
})
export class TagsModule {}
