import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'postgres',
    host: process.env.DB_HOST || 'docker-db-service',
    port: parseInt(<string>process.env.DB_PORT, 10) || 5432,
    username: process.env.DB_USERNAME || 'username',
    password: process.env.DB_PASSWORD || 'pasword',
    database: process.env.DB_NAME || 'databsename',
    entities: [__dirname + '/../**/*.entity.{ts,js}'],
    synchronize: process.env.NODE_ENV === 'production' ? false : true, // Turn of for Production
} 