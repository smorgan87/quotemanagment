import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/user.entity';
// import { TagRepository } from './tags.repository';
// import { GetTagFilterDto } from './dto/get-tag-filter-dto';
import { CreateNoteDto } from './dto/create-note.dto';
import { EditNoteDto } from './dto/edit-note.dto';
import { Quote } from '../quotes/quote.entity';
import { NoteRepository } from './note.repository';
// import { UpdateTagDto } from './dto/update-tag-dto';

@Injectable()
export class NotesService {
  constructor(
    @InjectRepository(NoteRepository)
    private noteRepository: NoteRepository,
  ) {}

  async createNote(
    createNoteDto: CreateNoteDto,
    quote: Quote,
    user: User,
  ){
    return this.noteRepository.createNote(createNoteDto, user, quote);
  }

  async editNote(
    editNoteDto: EditNoteDto,
  ){
    return this.noteRepository.editNote(editNoteDto);
  }

  async deleteNoteById(id: number, user: User): Promise<void> {
    const result = this.noteRepository.delete({
      id,
      userId: user.id,
    });
    if (!(await result).affected)
      throw new NotFoundException(`Note with id ${id} not found`);
  }

//   async getTagById(id: number, user: User): Promise<Tag> {
//   const tag = await this.tagRepository.findOne({
//   where: { id, userId: user.id },
//   });
//   if (!tag) throw new NotFoundException(`Tag with id id not found`);
//   return tag;
//   }

//   async createTag(createTagDto: CreateTagDto, user: User) {
//     return this.tagRepository.createTag(createTagDto, user);
//   }

//   async updateTag(id: number, updateTagDto: UpdateTagDto, user: User) {
//     const { name } = updateTagDto;
//     const tag = await this.tagRepository
//       .createQueryBuilder()
//       .update('tag')
//       .set({ name })
//       .where('id = :id', { id })
//       .execute();
//     return tag;
//   }

//   async deleteTagById(id: number, user: User): Promise<void> {
//     const result = await this.tagRepository.delete({
//       id,
//       userId: user.id,
//     });
//     if (!(await result).affected)
//       throw new NotFoundException(`Tag with id ${id} not found`);
//   }
}
