import { IsNotEmpty } from 'class-validator'

export class EditNoteDto{
    @IsNotEmpty()
    id: number;

    @IsNotEmpty()
    note: string;
}