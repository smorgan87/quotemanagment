import {
    BaseEntity,
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    OneToMany,
  } from 'typeorm';
  import { User } from 'src/auth/user.entity';
  import { Quote } from 'src/quotes/quote.entity';
  
  @Entity()
  export class Note extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    note: string;

    @Column()
    userId: number;
  
    @ManyToOne(
      type => User,
      user => user.note,
      { eager: false },
    )
    user: User;

    @ManyToOne(
      type => Quote,
      quote => quote.note,
      { eager: false, cascade: ["update"] },
    )
    quote: Quote;
  }
  