import { Repository, EntityRepository } from 'typeorm';
import { User } from 'src/auth/user.entity';
import { Note } from 'src/notes/note.entity';
import { Quote } from 'src/quotes/quote.entity';
import { CreateNoteDto } from 'src/notes/dto/create-note.dto';
import { EditNoteDto } from './dto/edit-note.dto';
import { Logger } from '@nestjs/common';

@EntityRepository(Note)
export class NoteRepository extends Repository<Note> {
  private logger = new Logger('TagRepository');

  async createNote(createNoteDto: CreateNoteDto, user: User, quote: Quote): Promise<Note> {
    const { note } = createNoteDto;
    this.logger.log(`
      Create note
        createNoteDto : ${JSON.stringify(createNoteDto)}
        quote         : ${JSON.stringify(quote)}
        user          : ${JSON.stringify(user)}
    `);
    const newNote = new Note();
    newNote.note = note;
    newNote.userId = user.id;
    newNote.quote = quote;
    this.logger.log(`
      Saving Note 
        createNoteDto : ${JSON.stringify(newNote)}
    `);
    await newNote.save();
    this.logger.log(`
      Created note 
        note : ${JSON.stringify(newNote)}
    `);
    return newNote;
  }

  async editNote(editNoteDto: EditNoteDto): Promise<Note> {
    const { note, id } = editNoteDto;
    this.logger.log(`
      Edit Note 
        editNoteDto : ${JSON.stringify(editNoteDto)}
    `);
    const editNote = new Note();
    editNote.note = note;
    editNote.id = id;
    this.logger.log(`
      Saving note 
        createNoteDto : ${JSON.stringify(editNote)}
    `);
    await editNote.save();
    // delete newNote.user; // Do Not Return User Data to Client
    // delete newNote.quote; // Do Not Return User Data to Client
    this.logger.log(`
      Edited Note note 
        note : ${JSON.stringify(editNote)}
    `);
    return editNote;
  }
}
