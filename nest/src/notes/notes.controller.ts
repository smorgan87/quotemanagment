import {
  Controller,
  Put,
  Query,
  ValidationPipe,
  Param,
  ParseIntPipe,
  UseGuards,
  Post,
  UsePipes,
  Body,
  Patch,
  Delete,
} from '@nestjs/common';
import { NotesService } from './notes.service';
import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/auth/user.entity';
import { EditNoteDto } from './dto/edit-note.dto';
import { Note } from './note.entity';
import { AuthGuard } from '@nestjs/passport';


@UseGuards(AuthGuard())
@Controller('notes')
export class NotesController {
  constructor(private notesService: NotesService) {}

  @Put('/')
  editNote(
    @Body() editNoteDto: EditNoteDto,
    // @GetUser() user: User,
  ) {
    return this.notesService.editNote(editNoteDto);
  }

  @Delete('/:id')
  deleteNoteById(
    @Param('id', ParseIntPipe) id: number,
    @GetUser() user: User,
  ): Promise<void> {
    return this.notesService.deleteNoteById(id, user);
  }

}
